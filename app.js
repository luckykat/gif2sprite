var isBuild = false;

window.onload = function () {
    var frameText = document.getElementById("frames");
    var hideOrigin = document.getElementById("hideOrigin");
    var header1 = document.getElementById("header1");
    var originX = document.getElementById("originX");
    var originY = document.getElementById("originY");
    var buttonPlay = document.getElementById("playButton");
    var buttonNext = document.getElementById("nextButton");
    var buttonTopLeft = document.getElementById("topleft");
    var buttonTop = document.getElementById("top");
    var buttonTopRight = document.getElementById("topright");
    var buttonLeft = document.getElementById("left");
    var buttonMiddle = document.getElementById("middle");
    var buttonRight = document.getElementById("right");
    var buttonBottomLeft = document.getElementById("bottomleft");
    var buttonBottom = document.getElementById("bottom");
    var buttonBottomRight = document.getElementById("bottomright");
    var select = document.getElementById("select");
    var zoomP = document.getElementById("zoomP");
    var controlsDiv = document.getElementById("controls");
    var speedSlider = document.getElementById("speedSlider");
    var speedInput = document.getElementById("speedInput");
    var saveButton = document.getElementById("saveButton");
    var fileDialog = document.getElementById("fileDialog");
    var canvas = document.getElementById('gifOutput');
    var bentoCanvas = document.getElementById('bento');
    var anchor = document.getElementById('download');
    var nameElement = document.getElementById('name');
    var paddingElement = document.getElementById('padding');
    var jsonText = document.getElementById('jsonText');
    var hasStartedBento = false;
    var hasAddedCursor = false;
    var gif = null;
    var globalWidth = 0;
    var globalHeight = 0;
    var entity;
    var currentFileName = '';
    var loadedFilePath = '';
    var loadedGif = false;
    var lastSavedPath = localStorage.getItem('lastSavedPath') || '';
    var isPlaying = 1;
    var gifs = {
        images: [],
        length: 0,
        animations: {}
    };
    var json = {};
    var addEventHandler = function (obj, evt, handler) {
        if (obj.addEventListener) {
            // W3C method
            obj.addEventListener(evt, handler, false);
        } else if (obj.attachEvent) {
            // IE method.
            obj.attachEvent('on' + evt, handler);
        } else {
            // Old school method.
            obj['on' + evt] = handler;
        }
    };
    var cancel = function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    };
    var loadImage = function (array, gifImage, filename, onReady) {
        var sup1 = new SuperGif({
            gif: gifImage,
            auto_play: false
        });
        sup1.load_raw(array, function () {

            // quick reset
            gifs = {
                images: [],
                length: 0,
                animations: {}
            };
            json = {};
            globalWidth = 0;

            gif = sup1;
            addGif(sup1, filename, onReady);
            // splitGif();
            // document.body.appendChild(sup1.get_canvas())
        });
    };
    var addGif = function (superGif, filename, onReady) {
        var i,
            img = superGif.get_canvas(),
            width = img.width,
            height = img.height,
            len = superGif.get_length(),
            n = len,
            m = 1,
            padding = parseInt(paddingElement.value),
            animation = {},
            start = gifs.length,
            canvas,
            context;

        // set
        if (!globalWidth) {
            globalWidth = width;
            globalHeight = height;
        } else {
            if (globalWidth !== width || globalHeight !== height) {
                alert('Wrong image size: ' + filename);
                return;
            }
        }

        animation.frames = [];
        animation.frameSpeeds = [];

        for (i = 0; i < n * m; ++i) {
            superGif.move_to(i);
            img = superGif.get_canvas();
            canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            context = canvas.getContext('2d');
            context.drawImage(
                img,
                0,
                0,
                width,
                height,
                0,
                0,
                width,
                height
            );
            //console.log(superGif.getDelay())

            // try to guess the speed
            // animation.speed = parseFloat((1 / (superGif.getDelay() / 100 * 60)).toFixed(2));
            // controlsDiv.innerHTML = animation.speed.toString();

            gifs.images.push(canvas);
            gifs.length += 1;
            animation.frames.push(start + i);
            animation.frameSpeeds.push(superGif.getDelay());
        }

        // normalize framespeeds based on fastest frame (lowest delay)
        var min = Infinity;
        for (i = 0; i < animation.frameSpeeds.length; ++i) {
            min = Math.min(animation.frameSpeeds[i], min);
        }
        for (i = 0; i < animation.frameSpeeds.length; ++i) {
            animation.frameSpeeds[i] = parseFloat((min / animation.frameSpeeds[i]).toFixed(2));
        }

        // try to guess the speed
        animation.speed = parseFloat((1 / (min / 100 * 60)).toFixed(2));
        speedSlider.value = animation.speed;
        speedInput.value = animation.speed;

        // gifs.animations[filename] = animation;
        gifs.animation = animation;

        onReady();

        // assume origin should be middle
        buttonBottom.click();
        previewBento();
    };
    var splitGif = function () {
        var i,
            img = gifs.images[0],
            len = gifs.length,
            width = img.width,
            height = img.height,
            n = len,
            m = 1,
            context,
            padding = parseInt(paddingElement.value);


        // try to make a square
        n = Math.ceil(Math.sqrt(len));
        m = Math.ceil(len / n);

        canvas.width = (width + padding) * n - padding;
        canvas.height = (height + padding) * m - padding;

        context = canvas.getContext('2d');
        for (i = 0; i < len; ++i) {
            context.drawImage(
                gifs.images[i],
                0,
                0,
                width,
                height, (i % n) * (width + padding),
                Math.floor(i / n) * (height + padding),
                width,
                height
            );
        }
        json = {
            origin: {
                x: 0,
                y: 0
            }
        };
        // json.imageName = nameElement.value || '';
        json.frameCountX = n;
        json.frameCountY = m;
        json.padding = padding;
        // json.animations = gifs.animations;
        json.animation = gifs.animation;

        setText();
    };
    var setText = function () {
        var jsonString;
        jsonString = JSON.stringify(json, null, 4);
        jsonText.innerHTML = jsonString;
        var blob = new Blob([jsonString], {
            type: "application/json"
        });
        var url = URL.createObjectURL(blob);

        anchor.download = (nameElement.value || 'animation') + ".json";
        anchor.href = url;
        anchor.textContent = "Download";

    };
    var previewBento = function () {
        updateOrigin();
        // zoomP.style.display = 'block';
        // preview in bento
        bentoCanvas.width = globalWidth;
        bentoCanvas.height = globalHeight;
        bentoCanvas.style.width = globalWidth * zoom + 'px';
        bentoCanvas.style.height = globalHeight * zoom + 'px';
        bento.require([
            'bento',
            'bento/entity',
            'bento/components/clickable',
            'bento/components/sprite',
            'bento/math/vector2',
            'bento/utils',
            'bento/packedimage'
        ], function (
            Bento,
            Entity,
            Clickable,
            Sprite,
            Vector2,
            Utils,
            PackedImage
        ) {
            // reset
            var spriteJson = Utils.cloneJson(json);
            var sprite;
            var packedImage = new PackedImage(canvas);
            // update frame text
            var frameTextUpdater = {
                name: 'frameTextUpdater',
                frameCache: 0,
                frameMaxCache: 0,
                update: function () {
                    var currentFrame = Math.floor(sprite.getCurrentFrame() + 1);
                    var leftNumber;
                    var digits;
                    if (this.frameCache !== currentFrame || this.frameMaxCache !== sprite.currentAnimationLength) {
                        this.frameCache = currentFrame;
                        this.frameMaxCache = sprite.currentAnimationLength;
                        // prepend spaces if needed
                        leftNumber = '       ' + this.frameCache;
                        digits = this.frameMaxCache.toString().length;
                        leftNumber = leftNumber.slice(-digits);
                        frameText.innerHTML = leftNumber + '/' + this.frameMaxCache;
                    }
                }
            };
            if (entity) {
                Bento.objects.remove(entity);
            }

            spriteJson.image = packedImage;
            spriteJson.animations = {
                default: spriteJson.animation
            };
            // ignore the origin
            spriteJson.origin = {
                x: 0,
                y: 0
            };
            sprite = new Sprite(spriteJson);

            entity = new Entity({
                name: 'preview',
                position: new Vector2(0, 0),
                originRelative: new Vector2(0, 0),
                components: [
                    sprite,
                    frameTextUpdater
                    // new Clickable({
                    //     pointerDown: function (data) {},
                    //     pointerUp: function (data) {},
                    //     pointerMove: function (data) {
                    //         entity.position.x = data.worldPosition.x / zoom;
                    //         entity.position.y = data.worldPosition.y / zoom;
                    //     }
                    // })
                ]
            });

            Bento.objects.attach(entity);

            updateSprite();
        });
    };
    var updateSprite = function () {
        // update current entity with speed change
        bento.require([
            'bento'
        ], function (
            Bento
        ) {
            Bento.objects.get('preview', function (preview) {
                var sprite = preview.getComponent('sprite', function (sprite) {
                    sprite.setCurrentSpeed(json.animation.speed * isPlaying);
                });
            });
        });
    };
    var nextFrame = function () {
        // update current entity with next frame
        bento.require([
            'bento'
        ], function (
            Bento
        ) {
            Bento.objects.get('preview', function (preview) {
                var sprite = preview.getComponent('sprite', function (sprite) {
                    sprite.currentFrame += 1;
                    sprite.currentFrame %= sprite.currentAnimation.frames.length;
                });
            });
        });
    };
    var updateOrigin = function () {
        // update origin entity
        bento.require([
            'bento'
        ], function (
            Bento
        ) {
            Bento.objects.get('origin', function (origin) {
                origin.visible = true;
                hideOrigin.checked = false;
                json.origin = json.origin || {
                    x: 0,
                    y: 0
                };
                origin.position.x = json.origin.x;
                origin.position.y = json.origin.y;
            });
        });
    };
    var saveImage = function (canvas, path) {
        var fs = window.rq('fs');
        var sys = window.rq('sys');
        // string generated by canvas.toDataURL()
        var img = canvas.toDataURL();
        // strip off the data: url prefix to get just the base64-encoded bytes
        var data = img.replace(/^data:image\/\w+;base64,/, "");
        var buf = new Buffer(data, 'base64');

        fs.writeFile(path, buf);
    };
    var saveSprite = function (evt) {
        // console.log(this.value);
        // remove any file extension
        var path = require('path');
        var fs = require('fs');
        var output = fileDialog.value;
        var parsedPath = path.parse(output);
        var outputFinal = path.join(parsedPath.dir, parsedPath.name);
        lastSavedPath = parsedPath.dir;
        localStorage.setItem('lastSavedPath', lastSavedPath);
        fileDialog.setAttribute('nwworkingdir', lastSavedPath);
        // console.log(outputFinal);

        // save json
        fs.writeFile(outputFinal + '.json', JSON.stringify(json, null, 4));
        // save image
        saveImage(canvas, outputFinal + '.png');

        // bug in nwjs's file dialog? 2nd time doesn't call any callback!
        // workaround: refresh file dialog
        fileDialog.parentNode.removeChild(fileDialog);
        // <input style="display:none;" id="fileDialog" type="file" nwsaveas/>
        fileDialog = document.createElement('input');
        fileDialog.setAttribute('style', "display:none;");
        fileDialog.setAttribute('type', "file");
        fileDialog.setAttribute('nwworkingdir', lastSavedPath);
        fileDialog.setAttribute('nwsaveas', currentFileName);
        fileDialog.addEventListener("change", saveSprite, false);
        document.body.appendChild(fileDialog);
    };
    var onDropFile = function (e) {
        var dt, files, i, reader, loaded = 0;
        var onFile = function (j) {
            var file = files[j];
            var filename;
            var loadGif = function () {
                var reader2 = new FileReader();
                var array = new Uint8Array(this.result);
                var onRead2 = function () {
                    loadImage(array, this.result, filename, function () {
                        loaded += 1;
                        if (loaded === files.length) {
                            splitGif();
                        }
                    });
                };

                //attach event handlers here...
                addEventHandler(reader2, 'loadend', onRead2);
                reader2.readAsDataURL(file);
            };
            var loadJson = function (evt) {
                var res = evt.target.result;
                var frameCountX = 0;
                var frameCountY = 0;
                var fs = require('fs');
                json = JSON.parse(res);

                // load corresponding png
                fs.readFile(file.path.replace(file.name, '') + filename + '.png', function (err, data) {
                    var png = new Image();
                    png.src = "data:image/jpeg;base64," + (new Buffer(data).toString('base64'));
                    png.onload = function () {
                        if (json.frameCountX) {
                            frameCountX = json.frameCountX;
                            globalWidth = (png.width - (json.padding || 0) * (json.frameCountX - 1)) / frameCountX;
                        } else if (json.frameWidth) {
                            globalWidth = json.frameWidth;
                        } else {
                            alert("JSON is not a sprite");
                            return;
                        }
                        if (json.frameCountY) {
                            frameCountY = json.frameCountY;
                            globalHeight = (png.height - (json.padding || 0) * (json.frameCountY - 1)) / frameCountY;
                        } else if (json.frameHeight) {
                            globalHeight = json.frameHeight;
                        } else {
                            alert("JSON is not a sprite");
                            return;
                        }

                        // draw on canvas
                        canvas.width = png.width;
                        canvas.height = png.height;
                        context = canvas.getContext('2d');
                        context.clearRect(0, 0, png.width, png.height);
                        context.drawImage(
                            png,
                            0,
                            0,
                            png.width,
                            png.height,
                            0,
                            0,
                            png.width,
                            png.height
                        );
                        // read origin
                        originX.value = json.origin.x;
                        originY.value = json.origin.y;
                        updateOrigin();
                        // read speed
                        speedSlider.value = json.animation.speed;
                        speedInput.value = json.animation.speed;

                        // load preview
                        previewBento();
                    };
                });
            };
            var reader = new FileReader();

            // must be gif or json
            if (file.type === "image/gif") {
                loadedGif = true;
                // saveButton.innerHTML = "Save Sprite";
                filename = file.name.replace('.gif', '');
                currentFileName = filename;
                loadedFilePath = file.path;
                header1.innerHTML = file.name;
                fileDialog.setAttribute('nwsaveas', filename);

                //attach event handlers here...
                addEventHandler(reader, 'loadend', loadGif);
                reader.readAsArrayBuffer(file);
                // reset origin
                originX.value = 0;
                originY.value = 0;
                updateOrigin();
                // show controls
                controlsDiv.style.display = "block";
            } else if (file.type === "application/json") {
                loadedGif = false;
                // saveButton.innerHTML = "Save JSON";
                filename = file.name.replace('.json', '');
                currentFileName = file.name;
                loadedFilePath = file.path;
                header1.innerHTML = file.name;
                fileDialog.setAttribute('nwsaveas', file.name);
                // assuming you want to overwrite the current json
                // fileDialog.setAttribute('nwworkingdir', loadedFilePath);
                // unload gif if needed
                gif = null;
                gifs = {
                    images: [],
                    length: 0,
                    animations: {}
                };
                // load json
                reader.onload = loadJson;
                reader.readAsText(file);
                // show controls
                controlsDiv.style.display = "block";
            } else if (file.type === "image/png") {
                // assume there is a json file
                loadedGif = false;
                filename = file.name.replace('.png', '');
                currentFileName = file.name;
                loadedFilePath = file.path;
                header1.innerHTML = file.name;
                fileDialog.setAttribute('nwsaveas', file.name.replace('.png', '.json'));
                // unload gif if needed
                gif = null;
                gifs = {
                    images: [],
                    length: 0,
                    animations: {}
                };
                // load json
                (function () {
                    var fs = require('fs');
                    var jsonFile = fs.readFileSync(file.path.replace(file.name, '') + filename + '.json', 'utf8');

                    loadJson({
                        target: {
                            result: jsonFile
                        }
                    });

                })();

                // show controls
                controlsDiv.style.display = "block";
            } else {
                alert("Not a valid file. Drag gif or json files.");
            }

            if (!lastSavedPath) {
                lastSavedPath = loadedFilePath;
                fileDialog.setAttribute('nwworkingdir', lastSavedPath);
            }
        };

        // get window.event if e argument missing (in IE)
        e = e || window.event;
        if (e.preventDefault) {
            e.preventDefault();
        } // stops the browser from redirecting off to the image.

        dt = e.dataTransfer;
        files = dt.files;

        for (i = 0; i < 1 /*files.length*/ ; i++) {
            onFile(i);
        }
        return false;
    };

    addEventHandler(nameElement, 'change', function () {
        json.imageName = nameElement.value;
        setText();
    });
    addEventHandler(paddingElement, 'change', splitGif);
    addEventHandler(document.body, 'dragover', cancel);
    addEventHandler(document.body, 'dragenter', cancel);
    addEventHandler(document.body, 'drop', onDropFile);

    // save spritesheet
    fileDialog.addEventListener("change", saveSprite, false);
    var zoom = 1;
    /**
     * Zoom
     */
    addEventHandler(document.body, 'wheel', function (e) {
        // one part is 120?
        var delta = e.deltaY;
        zoom -= (delta / 120) * 0.1;
        if (zoom < 0.1) {
            zoom = 0.1;
        }
        bentoCanvas.style.width = globalWidth * zoom + 'px';
        bentoCanvas.style.height = globalHeight * zoom + 'px';
        zoomP.innerHTML = "Zoom: " + zoom.toFixed(2);
        e.preventDefault();
    });

    /**
     * Speed slider
     */
    addEventHandler(speedSlider, 'change', function () {
        speedInput.value = speedSlider.value;
        json.animation.speed = speedSlider.value;
        updateSprite();
    }, false);
    addEventHandler(speedSlider, 'input', function () {
        speedInput.value = speedSlider.value;
        json.animation.speed = speedSlider.value;
        updateSprite();
    }, false);
    addEventHandler(speedInput, 'change', function () {
        speedSlider.value = parseFloat(speedInput.value);
        json.animation.speed = speedSlider.value;
        updateSprite();
    }, false);

    /**
     * Origin
     */
    addEventHandler(hideOrigin, 'change', function () {
        bento.require(['bento'], function (Bento) {
            Bento.objects.get('origin', function (origin) {
                origin.visible = !hideOrigin.checked;
            });
        });
    }, false);
    addEventHandler(originX, 'change', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = parseFloat(parseFloat(originX.value).toFixed(2));
        updateOrigin();
    }, false);
    addEventHandler(originY, 'change', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.y = parseFloat(parseFloat(originY.value).toFixed(2));
        updateOrigin();
    }, false);
    addEventHandler(buttonTopLeft, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = 0;
        json.origin.y = 0;

        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();

    }, false);
    addEventHandler(buttonTop, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = Math.floor(globalWidth / 2);
        json.origin.y = 0;
        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();
    }, false);
    addEventHandler(buttonTopRight, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = globalWidth;
        json.origin.y = 0;
        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();
    }, false);
    addEventHandler(buttonLeft, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = 0;
        json.origin.y = Math.floor(globalHeight / 2);
        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();
    }, false);
    addEventHandler(buttonMiddle, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = Math.floor(globalWidth / 2);
        json.origin.y = Math.floor(globalHeight / 2);
        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();
    }, false);
    addEventHandler(buttonRight, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = globalWidth;
        json.origin.y = Math.floor(globalHeight / 2);
        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();
    }, false);
    addEventHandler(buttonBottomLeft, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = 0;
        json.origin.y = globalHeight;
        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();
    }, false);
    addEventHandler(buttonBottom, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = Math.floor(globalWidth / 2);
        json.origin.y = globalHeight;
        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();
    }, false);
    addEventHandler(buttonBottomRight, 'click', function () {
        json.origin = json.origin || {
            x: 0,
            y: 0
        };
        json.origin.x = globalWidth;
        json.origin.y = globalHeight;
        // update text
        originX.value = json.origin.x;
        originY.value = json.origin.y;
        updateOrigin();
    }, false);

    /**
     * Preview play/stop button
     */
    addEventHandler(buttonPlay, 'click', function () {
        isPlaying = !isPlaying;
        updateSprite();

        if (isPlaying) {
            buttonPlay.innerHTML = "&#9724;";
        } else {
            buttonPlay.innerHTML = "&#9658;";
        }

    }, false);
    addEventHandler(buttonNext, 'click', function () {
        nextFrame();
    }, false);
    /**
     * Save button
     */
    addEventHandler(saveButton, 'click', function () {
        fileDialog.click();
    }, false);
    fileDialog.setAttribute('nwworkingdir', lastSavedPath);

    /**
     * select: for debugging
     */
    var effect = 'xor';
    addEventHandler(select, 'change', function () {
        effect = select.value;
    }, false);


    if (!isBuild) {
        require('nw.gui').Window.get().showDevTools();
    }

    // setup preview
    if (!hasStartedBento) {
        hasStartedBento = true;
        bento.require([
            'bento',
            'bento/math/rectangle',
            'bento/eventsystem',
            'bento/packedimage',
            'bento/entity',
            'bento/components/clickable',
            'bento/components/sprite',
            'bento/math/vector2',
            'bento/utils'
        ], function (
            Bento,
            Rectangle,
            EventSystem,
            PackedImage,
            Entity,
            Clickable,
            Sprite,
            Vector2,
            Utils
        ) {
            var canvasDimension = new Rectangle(0, 0, 32, 32);
            window.addEventListener('resize', function () {
                var viewport = Bento.getViewport();
                // since we're messing with css a lot: ignore offsetWidth and height
                viewport.width = bentoCanvas.offsetWidth;
                viewport.height = bentoCanvas.offsetHeight;
            }, false);

            Bento.setup({
                name: 'Gif2Sprite',
                canvasId: 'bento',
                canvasDimension: canvasDimension,
                renderer: 'canvas2d',
                manualResize: true,
                assets: {},
                pixelSize: 1,
                preventContextMenu: true,
                globalMouseUp: true,
                dev: true
            }, function () {
                // clear screen every tick
                var clearScreen = function () {
                    var canvasDimension = Bento.getViewport();
                    var clear = function (data) {
                        data.renderer.begin();
                        var context = data.renderer.getContext();
                        // context.clearRect(0, 0, globalWidth, globalHeight);
                        context.fillStyle = "#008080";
                        context.fillRect(0, 0, globalWidth, globalHeight);
                        data.renderer.flush();
                    };
                    EventSystem.on('preDraw', clear);
                };
                clearScreen();

                // load reticle
                var img = new Image();
                img.src = 'origin.png';
                img.onload = function () {
                    if (hasAddedCursor) {
                        return;
                    }
                    hasAddedCursor = true;

                    // load entity
                    var packedImage = new PackedImage(img);
                    var sprite = new Sprite({
                        originRelative: new Vector2(0.5, 0.5),
                        image: packedImage
                    });
                    var globalCompositeOperation = {
                        draw: function (data) {
                            var context = data.renderer.getContext();
                            context.globalCompositeOperation = 'difference';
                        },
                        postDraw: function (data) {
                            var context = data.renderer.getContext();
                            context.globalCompositeOperation = 'source-over';
                        }
                    };
                    var isDown = false;
                    var clickable = new Clickable({
                        pointerDown: function (data) {
                            isDown = true;
                            if (isDown && !hideOrigin.checked) {
                                cursor.position.x = Math.floor(data.worldPosition.x / zoom);
                                cursor.position.y = Math.floor(data.worldPosition.y / zoom);
                                originX.value = cursor.position.x;
                                originY.value = cursor.position.y;
                                json.origin.x = cursor.position.x;
                                json.origin.y = cursor.position.y;
                            }
                        },
                        pointerUp: function (data) {
                            isDown = false;
                        },
                        pointerMove: function (data) {
                            if (isDown && !hideOrigin.checked) {
                                cursor.position.x = Math.floor(data.worldPosition.x / zoom);
                                cursor.position.y = Math.floor(data.worldPosition.y / zoom);
                                originX.value = cursor.position.x;
                                originY.value = cursor.position.y;
                                json.origin.x = cursor.position.x;
                                json.origin.y = cursor.position.y;
                            }
                        }
                    });
                    var cursor = new Entity({
                        z: 1000,
                        name: 'origin',
                        position: new Vector2(0, 0),
                        // alpha: 0.5,
                        components: [
                            globalCompositeOperation,
                            sprite,
                            clickable
                        ]
                    });
                    cursor.visible = false;
                    Bento.objects.attach(cursor);
                };
            });
        });
    }
};