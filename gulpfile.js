var gulp = require('gulp');
var path = require('path');

gulp.task('clean', function () {
    var clean = require('gulp-clean');
    return gulp.src([
            'build',
            path.join('app', 'gif2sprite')
        ], {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});
gulp.task('copy', ['clean'], function () {
    // copy
    return gulp.src([
            './*.js',
            '!./gulpfile.js',
            './package.json',
            './index.html',
            './*.icns',
            './*.ico',
            './*.png',
            './style.css'
        ], {
            base: './'
        })
        .pipe(gulp.dest('./build'));
});
gulp.task('replace', ['copy'], function () {
    var replace = require('gulp-replace');
    // replace specific strings
    return gulp.src([
            './build/app.js'
        ], {
            base: './'
        })
        .pipe(replace('var isBuild = false;', 'var isBuild = true;'))
        .pipe(gulp.dest('./'));
});
gulp.task('default', ['copy', 'replace'], function (callback) {
    var NwBuilder = require('nw-builder');
    var nw = new NwBuilder({
        files: './build/**/**', // use the glob format
        platforms: ['osx64' , 'win32', 'win64'],
        buildDir: './app',
        macIcns: 'bento-dark-512.icns',
        version: '0.19.2',
        flavor: 'normal'
    });
    nw.build().then(function () {

        callback();
    }).catch(function (error) {
        console.error(error);
        callback();
    });
});

gulp.task('run', function (callback) {
    var exec = require('child_process').exec;
    exec('nw ./', {}, function (error, stdout, stderr) {});
});