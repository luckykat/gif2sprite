# Building Gif2Sprite

Before you can use Gif2Sprite, you must build it from the source. Steps:

1. Download this repo
2. Download NodeJS https://nodejs.org/
3. Install Gulp with `npm install -g gulp`
4. Install node modules in the project directory `npm install`
5. Run Gulp `gulp`